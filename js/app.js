

$(function() {
    // popup
    const popup = $('.popup');

    $('.popup .close').on('click', function () {
        popup.fadeOut()
    });

    function displayPopup() {
        popup.fadeIn();
    }

    /* Ротатор комментариев */
    const left = $('.arrow__left');
    const right = $('.arrow__right');
    const slides = $('.slide');
    const len = slides.length;
    slides.first().fadeIn();
    let current = 0;

    const pins = $('#pins').find('.pin');
    $(pins[current]).addClass('active');

    left.on("click", function() {
        $(this).attr('disabled', true);
        let that = $(this);
        $(slides[current]).fadeOut(function() {
            that.attr('disabled', true);
            current = current == 0 ? len - 1 : current - 1;
            $(slides[current]).fadeIn(function () {
                that.removeAttr('disabled');
                $(pins[current]).addClass('active').siblings().removeClass('active');
            });
        });
    });

    right.on("click", function() {
        $(this).attr('disabled', true);
        let that = $(this);
        $(slides[current]).fadeOut(function() {
            current = current == len - 1 ? 0 : current + 1;
            $(slides[current]).fadeIn(function () {
                that.removeAttr('disabled');
                $(pins[current]).addClass('active').siblings().removeClass('active');
            });
        });
    });

    pins.on('click', function () {
        const index = $(this).index();
        const that = $(this);
        $(slides[current]).fadeOut(function() {
            current = index;
            that.addClass('active').siblings().removeClass('active');
            $(slides[current]).fadeIn(function () {});
        });
    });

    /*Accordion*/
    var slideItems = $('.block_seven .item__header');
    slideItems.on('click', function () {
        $(this).parent().toggleClass('open')
    });

    $('.footer').text(`© ${new Date().getFullYear()}, Bechampions`);

    $('#sponsorSubmit').on('click', function (event) {
        event.preventDefault();
        const name = $('#sponsorName').val();
        const phone = $('#sponsorPhone').val();

        $.get(`http://bechampions.ru/mail/ajax.php?sendmail=1&name=${name}&phone=${phone}&type=2` )
            .done(function (data) {
                displayPopup();
            })
            .error(function (err) {
                console.log(err)
            })
    });

    $('#memberSubmit').on('click', function (event) {
        event.preventDefault();
        const name = $('#memberName').val();
        const phone = $('#memberPhone').val();

        $.get(`http://bechampions.ru/mail/ajax.php?sendmail=1&name=${name}&phone=${phone}&type=1` )
            .done(function (data) {
                displayPopup();
            })
            .error(function (err) {
                console.log(err)
            })
    });


    function onPrivacyPolicyClicked(evt) {
        var url = 'http://utslux.com/personal-data-agreement.html';
        var left = window.screen.availWidth / 2 - 240;
        var top = window.screen.availHeight / 2 - 320;
        evt.preventDefault();
        window.open(url, 'privacyPolicyWindow', 'width=480,height=640,toolbar=no,menubar=no,left='+left+',top='+top);
    }

    $('.link_wrapper a').on('click', onPrivacyPolicyClicked)

    $("header button").on('click', function (event) {
        event.preventDefault();
        var scroll_el = '#party-form';
        if ($(scroll_el).length != 0) {
            $('html, body').animate({ scrollTop: $(scroll_el).offset().top }, 500);
        }
    });


    $('.header__phone').on('click', function (event) {
        if (window.innerWidth > 700) return false;

        if (!$(event.target).hasClass('mphone')) {
            $(this).toggleClass('open');
        }
    });

    const shim = $('.block_nine .shim > span');
    const hiddens = $('.block_nine .hidden');

    shim.on('click', function () {

        if (!$(this).hasClass('open')) {
            hiddens.show();
            $(this).text('Скрыть')
        } else {
            hiddens.hide();
            $(this).text('Читать полностью')
        }

        $(this).toggleClass('open').parent().toggleClass('open');
    });

});

